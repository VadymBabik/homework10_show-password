"use strict";
const form = document.querySelector(".password-form");
//const btn = document.querySelector(".btn");
const getError = document.createElement("p");
getError.textContent = `Нужно ввести одинаковые значения`;
getError.style.color = "red";
btn.addEventListener("click", (e) => {
  e.preventDefault();
  let input = document.querySelectorAll("input");
  if (input[0].value && input[1].value) {
    if (input[0].value === input[1].value) {
      getError.remove();
      alert("You are welcome");
    } else {
      document.querySelector("button").before(getError);
    }
  }
  // return false;
});
let show = document.querySelectorAll(".icon-password");
show.forEach((elem) => {
  elem.addEventListener("click", (e) => {
    let input = e.target.parentNode.querySelector("input");
    if (input.getAttribute("type") === "password") {
      input.setAttribute("type", "text");
      e.target.classList.remove("fa-eye");
      e.target.classList.add("fa-eye-slash");
    } else {
      input.setAttribute("type", "password");
      e.target.classList.remove("fa-eye-slash");
      e.target.classList.add("fa-eye");
    }
  });
});
